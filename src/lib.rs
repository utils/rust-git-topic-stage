// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![warn(missing_docs)]
// XXX(rust-1.66)
#![allow(clippy::uninlined_format_args)]

//! Git Topic Stage
//!
//! This library implements a staging workflow to queue candidate topic branches into a single
//! integration branch. Candidate topics are merged together into an integration branch. Topics
//! which have been merged previously and recieve a new update are removed from the set and placed
//! back at the end of the list of topics to be merged. This way a single topic branch cannot cause
//! starvation in the rest of the topics.
//!
//! See also [`gitworkflow`][gitworkflow].
//!
//! [gitworkflow]: https://github.com/rocketraman/gitworkflow

mod stager;

pub use stager::*;

#[cfg(test)]
mod test;
