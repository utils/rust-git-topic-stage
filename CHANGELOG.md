# v4.1.2

  * MSRV bumped to 1.63. Minor updates were made in the meantime, but
    dependency updates have forced this. "Mindeps" has an MSRV of 1.63.

# v4.1.1

  * MSRV bumped to 1.60. Minor updates were made in the meantime, but
    dependency updates have forced this. "Mindeps" has an MSRV of 1.53.

# v4.1.0

  * MSRV bumped to 1.45. While the code update is just for a small code
    simplification, in order to simplify CI, updating makes sense.

# v4.0.0

  * Remove methods deprecated in 2.0.0.
  * Remove the `identity` from the `Stager` object. The committer information
    comes from the configuration file provided by the given context.

## API changes

  * `Topic::new` now uses `Into<String>` rather than `ToString` for the `name`
    and `url` parameters. This may cause ownership to be transferred where it
    would not have before. Use `Clone` to keep ownership if necessary.

# v3.0.0

  * Bump `chrono` from `0.3` to `0.4`. This is required for compatibility with
    `serde-1.0`.

# v2.0.0

  * Updated to `git-workarea-2.0.0`.
  * Deprecated `OldTopicRemoval::topic()` and `StagedTopic::topic()` in
    preference for direct access to the member.

# v1.0.0

  * Initial stable release.
