# git-topic-stage

This crates implements a staging branch workflow where a base branch may have
multiple proposed topics ready for testing at once so that the combination may
be tested together when testing resources are not available for testing every
proposed change individually.

The setup is that branches are added to the set of topics which will be
prepared onto an integration branch. When any branch in the set changes, the
integration branch is rewritten. Any topic which fails to merge cleanly is
removed from the set of candidate topics. Once a new integration branch is
ready, the set of topics merged and rejected are returned for processing.

This is also known as:

- "next" in `git.git`
- "merge trains" on GitLab
- [`gitworkflow`][gitworkflow]

[gitworkflow]: https://github.com/rocketraman/gitworkflow
